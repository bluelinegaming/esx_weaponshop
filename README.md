# esx_weaponshop
Modified by @diveyez for BlueLineGaming FiveM Server

## Tracked Changes:
      - Modified server/main.lua to change amount of ammo given on purchase 9/16/2020
              **Example** https://forum.cfx.re/t/help-editing-esx-weaponshop-buying-ammo/467514/10
## Warning
 Be sure you backup any files before replacing them with this.
